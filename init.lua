require("my-utils")
require("settings")
require("mappings")

_G.plugins = {}
require("plugins")

-- bootstrap packer {
local execute = vim.api.nvim_command
local install_path = vim.fn.stdpath('data')..'/site/pack/packer/opt/packer.nvim'

if vim.fn.empty(vim.fn.glob(install_path)) > 0 then
  execute('!git clone https://github.com/wbthomason/packer.nvim '..install_path)
end

vim.cmd [[packadd packer.nvim]]

-- see ./plugins/init.lua for definition
-- require('packer').startup(_G.plugins)
-- }
require('packer').startup(function()
  for _, p in ipairs(_G.plugins) do
    require('packer').use(p)
  end
end)

-- update init.lua
-- vim.cmd([[autocmd BufWritePost init.lua luafile ~/.config/nvim/init.lua]])
-- somehow freezes vim

-- nice autommcands
-- remove trailing whitespaces
-- vim.cmd([[autocmd BufWritePre * %s/\s\+$//e]])

-- higlight trailing spaces
vim.cmd([[highlight ExtraWhitespace ctermbg=LightGray guibg=LightGray]])
vim.cmd([[match ExtraWhitespace /\s\+$/]])

-- remove trailing newline
vim.cmd([[autocmd BufWritePre * %s/\n\+\%$//e]])
