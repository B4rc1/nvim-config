function use(plug)
  table.insert(_G.plugins, plug)
end

map = vim.api.nvim_set_keymap
unmap = vim.api.nvim_del_keymap

function mapleader(key, command)
	map("n", "<Leader>" .. key, command, {noremap = true, silent = true})
end

function file_exists(name)
   local f=io.open(name,"r")
   if f~=nil then io.close(f) return true else return false end
end
