use { 'habamax/vim-godot' }

use { 'weilbith/nvim-code-action-menu',
  config = function ()
	  mapleader('ca', '<cmd>CodeActionMenu<CR>')
  end
}
use { 'kosayoda/nvim-lightbulb',
  config = function ()
    vim.cmd [[autocmd CursorHold,CursorHoldI,CursorMoved,InsertLeave * lua require'nvim-lightbulb'.update_lightbulb()]]
    vim.fn.sign_define('LightBulbSign', { text = "🌳", texthl = "", linehl="", numhl="" })
  end
}

use { 'neovim/nvim-lspconfig',
	requires = {
		'RRethy/vim-illuminate',
		'ray-x/lsp_signature.nvim',
		'folke/lua-dev.nvim',
		'hrsh7th/cmp-nvim-lsp',
	},
	config = function()

	-- vim-illuminate
	vim.g.Illuminate_ftblacklist = { "help" }

	local servers = { 'clangd', 'pyright', "gopls" }

	-- lsp
	local nvim_lsp = require('lspconfig')
	local on_attach = function(client, bufnr)
		require'lsp_signature'.on_attach()

	  vim.api.nvim_buf_set_option(bufnr, 'omnifunc', 'v:lua.vim.lsp.omnifunc')

	  local opts = { noremap=true, silent=true }
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gt', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'R', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
	  -- vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>ca', '<cmd>lua vim.lsp.buf.code_action()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<leader>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<c-p>', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
	  vim.api.nvim_buf_set_keymap(bufnr, 'n', '<c-n>', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)

		require 'illuminate'.on_attach(client)
	end

	local capabilities = require('cmp_nvim_lsp').update_capabilities(vim.lsp.protocol.make_client_capabilities())

	-- paru -S lua-language-server
	local luadev = require("lua-dev").setup({
		lspconfig = {
			cmd = {"lua-language-server"},
			settings = {
				Lua = {
					runtime = {
						path = {
							"?.lua",
							"?/?.lua",
							"?/init.lua",
							"?/?/init.lua"
						}
					},
					workspace = {
						preloadFileSize = 500,
					}
				}
			},
			on_attach = on_attach,
		},
	})

	nvim_lsp.sumneko_lua.setup(luadev)

	nvim_lsp.rust_analyzer.setup {
		cmd = { "rust-analyzer" },
	  filetypes = { "rust" },
	  root_dir = nvim_lsp.util.root_pattern("Cargo.toml", "rust-project.json"),
		settings = {
			["rust-analyzer"] = {
				completion = {
					addCallParenthesis = true,
				},
			}
		},
		on_attach = on_attach,
		capabilities = capabilities,
	}

	-- workaround godot until 3.3.3 gets released:
	-- https://github.com/godotengine/godot/pull/50277
  nvim_lsp.gdscript.setup {
    on_attach = function (client, bufnr)
      on_attach(client, bufnr)

      local _notify = client.notify
      client.notify = function (method, params)
          if method == 'textDocument/didClose' then
              return
          end
          _notify(method, params)
      end
    end
  }


  local workspace_dir = vim.fn.fnamemodify(vim.fn.getcwd(), ':p:h:t')
  require'lspconfig'.jdtls.setup{
      cmd = { 'jdtls', '-data', "/home/jonas/.cache/jdtls/" .. workspace_dir },
      capabilities = capabilities,
      on_attach = on_attach,
      root_dir = nvim_lsp.util.find_git_ancestor
    }

	for _, lsp in ipairs(servers) do
	  nvim_lsp[lsp].setup { capabilities = capabilities; on_attach = on_attach,  }
	end
end }-- LSP with nvim 0.5
