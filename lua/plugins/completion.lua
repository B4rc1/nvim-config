vim.o.completeopt = "menu,menuone,noselect"
vim.opt.lazyredraw = true

use { 'hrsh7th/nvim-cmp',
	requires = {
		'hrsh7th/cmp-buffer',
    'hrsh7th/cmp-cmdline',
    'hrsh7th/cmp-path',
		'hrsh7th/cmp-nvim-lsp',
		'hrsh7th/cmp-vsnip',
		'SirVer/ultisnips',
		'quangnguyen30192/cmp-nvim-ultisnips'
  },
	config = function ()
		-- helper functions for mappings
		local has_words_before = function()
			local line, col = unpack(vim.api.nvim_win_get_cursor(0))
			return col ~= 0 and vim.api.nvim_buf_get_lines(0, line - 1, line, true)[1]:sub(col, col):match("%s") == nil
		end

		local press = function(key)
      vim.api.nvim_feedkeys(vim.api.nvim_replace_termcodes(key, true, true, true), "n", true)
		end

		-- Setup nvim-cmp.
		local cmp = require'cmp'

		-- according to https://github.com/quangnguyen30192/cmp-nvim-ultisnips:
		-- "We have to disable this by set UltiSnipsRemoveSelectModeMappings = 0 (Credit JoseConseco)"
		vim.g.UltiSnipsRemoveSelectModeMappings = 0
		vim.g.UltiSnipsEditSplit = "context"
		vim.g.UltiSnipsEnableSnipMate = false
		-- TODO: :UltiSnipsAddFiletypes tex.md for markdown files


		cmp.setup({
      snippet = {
        expand = function(args)
          vim.fn["UltiSnips#Anon"](args.body)
        end,
      },
			mapping = {
      ['<C-r>'] = cmp.mapping(cmp.mapping.scroll_docs(-4), { 'i', 'c' }),
      ['<C-t>'] = cmp.mapping(cmp.mapping.scroll_docs(4), { 'i', 'c' }),
				-- ['<C-Space>'] = cmp.mapping.complete(),
       [ '<C-e>'] = cmp.mapping({
        i = cmp.mapping.abort(),
        c = cmp.mapping.close(),
        }),
				['<CR>'] = cmp.mapping.confirm({ select = true }),
        ['<C-Space>'] = cmp.mapping(function (fallback)
          if cmp.visible() then
            cmp.select_next_item()
          else
            fallback()
          end
        end, {"i", "c"}),
				["<Tab>"] = cmp.mapping(function(fallback)
					if vim.fn.complete_info()["selected"] == -1 and vim.fn["UltiSnips#CanExpandSnippet"]() == 1 then
            press("<C-R>=UltiSnips#ExpandSnippet()<CR>")
          elseif vim.fn["UltiSnips#CanJumpForwards"]() == 1 then
            press("<ESC>:call UltiSnips#JumpForwards()<CR>")
          elseif cmp.visible() then
            cmp.select_next_item()
					elseif has_words_before() then
						press("<Tab>")
					else
						fallback()
					end
				end, { "i", "s", "c" }),

				["<S-Tab>"] = cmp.mapping(function(fallback)
					if vim.fn["UltiSnips#CanJumpBackwards"]() == 1 then
            press("<ESC>:call UltiSnips#JumpBackwards()<CR>")
          elseif cmp.visible() then
            cmp.select_prev_item()
          else
            fallback()
					end
				end, { "i", "s", "c" }),
			},
			sources = {
				{ name = 'nvim_lsp' },
        { name = 'path' },
				{ name = 'ultisnips' },
				{ name = 'buffer' },
			},
      experimental = {
        ghost_text = true,
      }
		})
    -- Use buffer source for `/`.
    cmp.setup.cmdline('/', {
      sources = {
        { name = 'buffer' }
      }
    })

    -- Use cmdline & path source for ':'.
    cmp.setup.cmdline(':', {
      sources = cmp.config.sources({
        { name = 'path' }
      }, {
        { name = 'cmdline' }
      })
    })

	end
}
