require("plugins.statusline")
require("plugins.telescope")
require("plugins.treesitter")
require("plugins.lsp")
require("plugins.completion")
require("plugins.trouble")

use { 'wbthomason/packer.nvim', opt = true } -- my plugin manager
-------------------------------------
-- use { 'bluz71/vim-nightfly-guicolors', config= [[vim.cmd "colorscheme nightfly"]] } -- my colorscheme
use({
    'rose-pine/neovim',
    as = 'rose-pine',
    config = function()
        -- Options (see available options below)
        vim.g.rose_pine_variant = 'base'

        -- Load colorscheme after options
        vim.cmd('colorscheme rose-pine')
    end
})
-------------------------------------
use { 'airblade/vim-rooter', config = function()
	vim.g.rooter_silent_chdir = true
	vim.g.rooter_patterns = {'.git', 'Makefile', '*.sln', 'build/env.sh'}
end } -- autocd
-------------------------------------
use {
	'windwp/nvim-autopairs',
  require = { 'hrsh7th/nvim-cmp' },
	config = function ()
		local npairs = require('nvim-autopairs')
		local Rule   = require'nvim-autopairs.rule'
		npairs.setup({
			map_c_w = true,
			ignored_next_char = "[%w%.]" -- will ignore alphanumeric and `.` symbol
		})

		-- add spaces between parentheses
		npairs.add_rules {
			Rule(' ', ' ')
				:with_pair(function (opts)
					local pair = opts.line:sub(opts.col - 1, opts.col)
					return vim.tbl_contains({ '()', '[]', '{}' }, pair)
				end),
			Rule('( ', ' )')
					:with_pair(function() return false end)
					:with_move(function(opts)
							return opts.prev_char:match('.%)') ~= nil
					end)
					:use_key(')'),
			Rule('\\{', '\\}', 'tex'),
			Rule('{ ', ' }')
					:with_pair(function() return false end)
					:with_move(function(opts)
							return opts.prev_char:match('.%}') ~= nil
					end)
					:use_key('}'),
			Rule('[ ', ' ]')
					:with_pair(function() return false end)
					:with_move(function(opts)
							return opts.prev_char:match('.%]') ~= nil
					end)
					:use_key(']')
		}

		local cmp_autopairs = require("nvim-autopairs.completion.cmp")
    local cmp = require('cmp')
    cmp.event:on('confirm_done', cmp_autopairs.on_confirm_done({ map_char = { tex = '' } }))
	end
}
-------------------------------------
use {
    'numToStr/Comment.nvim',
    config = function()
        require('Comment').setup()
				local ft = require('Comment.ft')
        ft.set("nix", {'#%s', '/*%s*/'})
    end
}
-------------------------------------
use {'norcalli/nvim-colorizer.lua', config = [[require"colorizer".setup()]]} -- colorize text like #ff0 or #ff0000
-------------------------------------
use { 'lewis6991/gitsigns.nvim',         -- Very interesting seeming git plugin. TODO: check this out
  requires = { 'nvim-lua/plenary.nvim' },
	config = [[require('gitsigns').setup()]]
}
-------------------------------------
use {'plasticboy/vim-markdown',
  require = {'godlygeek/tabular'},
  config = function ()
    vim.g.vim_markdown_math = true
  end,
}
-------------------------------------
use {'mbbill/undotree', config = function ()
  -- open undo
	mapleader("ou", "<Cmd>UndotreeToggle<CR><Cmd>UndotreeFocus<CR>")
end}
-------------------------------------
use {
  'ggandor/lightspeed.nvim',
  requires = {'tpope/vim-repeat'},
  config = function ()
    -- restore native f/F/t/T
    vim.cmd([[
      map <C-M-S-F11> <Plug>Lightspeed_f
      map <C-M-S-F12> <Plug>Lightspeed_F
      map <C-M-S-F10> <Plug>Lightspeed_t
      map <C-M-S-F9> <Plug>Lightspeed_T
    ]])

    require'lightspeed'.setup {
        full_inclusive_prefix_key = '<space>',
        labels = {'h', "i", "e", "a", "o", "d", "t", "r", "n", "s",
                  "H", "I", "E", "A", "O", "D", "T", "R", "N", "S", }
    }
  end
}
-------------------------------------
use {
  "folke/zen-mode.nvim",
  config = function()
    require("zen-mode").setup {
    }
    mapleader("tz", "<Cmd>ZenMode<CR>")
  end
}
-------------------------------------
use {
  "folke/which-key.nvim",
  config = function()
    local presets = require("which-key.plugins.presets")
    presets.operators["d"] = nil

    require("which-key").setup {
      -- your configuration comes here
      -- or leave it empty to use the default settings
      -- refer to the configuration section below
      operators = { gc = "Comments" },
			triggers_blacklist = {
				i = { ",", "," },
			},
    }
  end
}
-------------------------------------
use {'Olical/conjure', tag="v4.21.0"}
-------------------------------------
use { 'eraserhd/parinfer-rust', run = 'cargo build --release' }
-------------------------------------
use {
	'kdheepak/lazygit.nvim',
	config = function ()
		mapleader("gg", ":LazyGit<CR>")
	end
}
-------------------------------------
use {
	"akinsho/toggleterm.nvim",
	config = function ()
		require("toggleterm").setup{
			hide_numbers = false,
			shade_terminals = false,
			start_in_insert = true,
			direction = 'horizontal',
		}
		mapleader("tt", "<Cmd>ToggleTerm<CR>")
	end
}
-------------------------------------
use {
  'ptzz/lf.vim',
	requires = { 'voldikss/vim-floaterm' },
	config = function ()
		vim.g.lf_map_keys = false
		vim.g.lf_replace_netrw = true
		vim.g.lf_width = 0.8
		vim.g.lf_height = 0.8
		mapleader("ff", "<Cmd>Lf<CR>")
	end
}
-------------------------------------
use {
	'David-Kunz/treesitter-unit',
	config = function ()
		map('x', 'iu', ':lua require"treesitter-unit".select()<CR>', {noremap=true})
		map('x', 'au', ':lua require"treesitter-unit".select(true)<CR>', {noremap=true})
		map('o', 'iu', ':<c-u>lua require"treesitter-unit".select()<CR>', {noremap=true})
		map('o', 'au', ':<c-u>lua require"treesitter-unit".select(true)<CR>', {noremap=true})
	end
}
-------------------------------------
use {
	'andymass/vim-matchup',
}
-------------------------------------
use {
	"luukvbaal/stabilize.nvim",
	config = function() require("stabilize").setup() end
}
