use {
    'nvim-telescope/telescope.nvim',
    requires = {{'nvim-lua/popup.nvim'}, {'nvim-lua/plenary.nvim'}},
    config = function()

    local actions = require('telescope.actions')

    require('telescope').setup{
      defaults = {
        vimgrep_arguments = {
          'rg',
          '--color=never',
          '--no-heading',
          '--with-filename',
          '--line-number',
          '--column',
          '--smart-case'
        },
        prompt_prefix = "> ",
        selection_caret = " ",
        entry_prefix = "  ",
        initial_mode = "insert",
        selection_strategy = "reset",
        sorting_strategy = "descending",
        layout_strategy = "horizontal",
				layout_config = {
          horizontal = {
            mirror = false,
          },
          vertical = {
            mirror = false,
          },
				},
        file_sorter =  require'telescope.sorters'.get_fuzzy_file,
        file_ignore_patterns = {},
        generic_sorter =  require'telescope.sorters'.get_generic_fuzzy_sorter,
        winblend = 0,
    		results_title = false,
        border = {},
    		-- borderchars = {
          -- { '─', '│', '─', '│', '╭', '╮', '╯', '╰'},
          -- prompt = {"─", "│", " ", "│", "╭", "╮", "│", "│"},
          -- results = {"─", "│", "─", "│", "├", "┤", "╯", "╰"},
          -- preview = { '─', '│', '─', '│', '╭', '╮', '╯', '╰'},
        -- },
        color_devicons = true,
        use_less = true,
        set_env = { ['COLORTERM'] = 'truecolor' }, -- default = nil,
        file_previewer = require'telescope.previewers'.vim_buffer_cat.new,
        grep_previewer = require'telescope.previewers'.vim_buffer_vimgrep.new,
        qflist_previewer = require'telescope.previewers'.vim_buffer_qflist.new,
    		mappings = {
    			i = {
    				["C-j"] = actions.move_selection_next,
    				["C-k"] = actions.move_selection_previous,
    			},
    			n = {
    				["C-c"] = actions.close,
    			}
    		},

        -- Developer configurations: Not meant for general override
        buffer_previewer_maker = require'telescope.previewers'.buffer_previewer_maker
      }
    }

    -- Telescope
    function search_nvim_config()
        require("telescope.builtin").find_files(
            {
                prompt_title = "Neovim Config",
                cwd = "$HOME/.config/nvim/"
            }
        )
    end


    map("n", "z=", "<Cmd>:Telescope spell_suggest<CR>", { noremap = true, silent = true })
    map("n", "gr", "<Cmd>:Telescope lsp_references<CR>", { noremap = true, silent = true })

    mapleader("<space>", "<Cmd>:Telescope find_files<CR>")
    mapleader("?", "<Cmd>:Telescope keymaps<CR>")
    mapleader("h:", "<Cmd>:Telescope help_tags<CR>")
    mapleader("bb", "<Cmd>:Telescope buffers<CR>")
    mapleader("fg", "<Cmd>:Telescope live_grep<CR>")
    mapleader("fc", "<Cmd>:lua search_nvim_config()<CR>")

    end
} -- fzf replacement
