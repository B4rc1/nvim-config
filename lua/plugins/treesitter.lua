use {
	'nvim-treesitter/nvim-treesitter',
	requires = {'nvim-treesitter/playground'},
	config = function()
		require'nvim-treesitter.configs'.setup {
			ensure_installed = "maintained", -- one of "all", "maintained" (parsers with maintainers), or a list of languages
			highlight = { enable = true },
			incremental_selection = { enable = false },
			textobjects = { enable = true },
			indent = { enable = true },
			matchup = { enable = true }
		}
	end
} -- Better Syntax highlighting with nvim 0.5


use { 'lukas-reineke/indent-blankline.nvim' } -- indent guidelines, depends (with my config) on treesitter

vim.g.indent_blankline_use_treesitter = true
vim.g.indent_blankline_char_list  =  {'|', '¦', '┆', '┊'}

vim.g.indent_blankline_use_treesitter = true
vim.g.indent_blankline_show_current_context = true

vim.g.indent_blankline_show_end_of_line = true
vim.g.indent_blankline_filetype_exclude = { 'help' }
vim.g.indent_blankline_buftype_exclude = { 'terminal', 'nofile', 'packer'}
-- vim.g.indent_blankline_char_highlight = 'LineNr'
vim.g.indentLine_setColors = false
