-- always ask to confirm if file modified
vim.o.confirm = true

--Case insensitive searching UNLESS /C or capital in search
vim.o.ignorecase = true
vim.o.smartcase = true

-- enable mouse support
vim.o.mouse = "a"

-- preview subsitutions
vim.o.inccommand = "nosplit"

-- use system clipboard as default register
vim.o.clipboard = "unnamedplus"

-- automatically write the file if switchting files
vim.o.autowrite = true

-- highlight yank
vim.cmd([[au TextYankPost * silent! lua vim.highlight.on_yank {higroup = 'visual'}]])

-- permanent undo
vim.bo.undofile = true

-- tabs as 2 spaces
vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.expandtab = true
vim.opt.softtabstop = 2

-- mix of relative and absolute line numbers
vim.wo.number = true
vim.wo.relativenumber = true

-- fold by marker and don't fold by default
vim.wo.foldmethod='marker'
vim.wo.foldlevel=99999
-- nicer folds
vim.cmd([[set foldtext=substitute(getline(v:foldstart),'\\t',repeat('\ ',&tabstop),'g').'...'.trim(getline(v:foldend))]])

-- use "real" colors instead of terminal colors
vim.o.termguicolors = true

-- hide --INSERT-- under the status bar
vim.o.showmode = false
vim.cmd([[filetype indent off]])

-- better tex support
vim.g.tex_flavor = "latex"
