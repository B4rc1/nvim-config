require("my-utils")

vim.opt.tabstop = 2
vim.opt.shiftwidth = 2
vim.opt.softtabstop = 2

function execute_love2d()
		vim.cmd([[TermExec cmd="love ."]])
end

map("n", "ü", [[<Cmd>lua execute_love2d()<CR>]], { noremap = true })
