require("my-utils")

vim.opt.colorcolumn = "100"

function execute_python()
	if file_exists("main.py") then
		vim.cmd([[TermExec cmd="python3 main.py"]])
	else
		vim.cmd([[TermExec cmd="python3 %"]])
	end
end

map("n", "ü", [[<Cmd>lua execute_python()<CR>]], { noremap = true })
